################################################################
# METHOD 11 details: 
# Trained on data config 5 - RGB images converted to pose
# estimation images using MediaPipe.
# If images could not be converted, raw images used instead.
# Only last layer is trained/use as feature extraction
# Whole model has been fine-tuned not just last layer as in 8
# Code (in part) developed with reference to the following resource: https://github.com/LeanManager/PyTorch_Image_Classifier
################################################################


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2
from collections import OrderedDict, Counter
import os
import glob
from PIL import Image
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix, ConfusionMatrixDisplay

import torch
from torch import nn
from torch import optim
from torch.utils.data.dataset import Dataset
from torchvision import datasets, transforms, models


os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

#Empty gpu cache
torch.cuda.empty_cache()



################################################################
#Defining the data transforms                                
#Normalised means and stds are based on the model pretraining
################################################################

training_transforms = transforms.Compose([transforms.ToPILImage(),
                                          transforms.Resize((120, 160)),
                                          transforms.ToTensor(),
                                          transforms.Normalize([0.485, 0.456, 0.406], 
                                                               [0.229, 0.224, 0.225])])

validation_transforms = transforms.Compose([transforms.ToPILImage(),
                                            transforms.Resize((120, 160)),  
                                            transforms.ToTensor(),
                                            transforms.Normalize([0.485, 0.456, 0.406], 
                                                                 [0.229, 0.224, 0.225])])



################################################################
#Defining train/val/test sets
################################################################

data_dir = "ISL-mediapipe/Frames-colour/"

train_files = sorted(glob.glob(os.path.join(data_dir, "mediapipe-Person1*")) + glob.glob(os.path.join(data_dir, "mediapipe-Person2*")))
val_files = sorted(glob.glob(os.path.join(data_dir, "mediapipe-Person3*")) + glob.glob(os.path.join(data_dir, "mediapipe-Person4*")))
test_files = sorted(glob.glob(os.path.join(data_dir, "mediapipe-Person5*")) + glob.glob(os.path.join(data_dir, "mediapipe-Person6*")))

# The split outputs this: 
# ['ISL', 'master/Frames/Person1/Person1', 'A', '1', '1.jpg'], 
# the 3rd index (i.e. index 2) gives the label
train_labels = [file_name.split('-')[4] for file_name in train_files]
val_labels = [file_name.split('-')[4] for file_name in val_files]
test_labels = [file_name.split('-')[4] for file_name in test_files]

# Make sure you don't have an overlap in files between sets
assert set(train_files) & set(val_files) == set()
assert set(test_files) & set(val_files) == set()
assert set(train_files) & set(test_files) == set()

classes = list(Counter(train_labels).keys())



################################################################
#Creating dict fot class indices
################################################################

idx_to_class = {i:j for i, j in enumerate(classes)}
class_to_idx = {value:key for key,value in idx_to_class.items()}



################################################################
#Defining the dataset class
################################################################

class ISLHandsignDataset(Dataset):
    def __init__(self, image_paths, labels, transform):
        """
        image_paths (str): Path to image file. 
        """
        
        self.image_paths = image_paths
        self.labels = labels
        self.transform = transform

    def __len__(self):
        return len(self.image_paths)
    
    def __getitem__(self, idx):
        image_filepath = self.image_paths[idx]
        image = cv2.imread(image_filepath)
        image = self.transform(image)
        
        label = self.labels[idx]
        label = class_to_idx[label]
        
        return image, label
    
    

################################################################
#Loading the datasets
################################################################

training_dataset = ISLHandsignDataset(train_files, train_labels, transform=training_transforms)
validation_dataset = ISLHandsignDataset(val_files, val_labels, transform=validation_transforms)



################################################################
#Defining data loaders
################################################################

train_loader = torch.utils.data.DataLoader(training_dataset, batch_size=64, shuffle=True)
val_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=32)



################################################################
#Building and training the classifier
################################################################

model = models.vgg16(pretrained=True)

for parameter in model.parameters():
    parameter.requires_grad = True    #CHANGE HERE
    

#Custom classifier:
classifier = nn.Sequential(OrderedDict([('fc1', nn.Linear(25088, 4000)),   
                                       ('relu', nn.ReLU()),                 
                                       ('drop', nn.Dropout(p=0.5)),  
                                       ('fc2', nn.Linear(4000, 26)), 
                                       ('output', nn.LogSoftmax(dim=1))])) 

model.classifier = classifier


#Check if a GPU is available and if not use cpu
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


#Set optimizer
optimizer = optim.Adam(model.classifier.parameters(), lr=0.0001)


#Set loss/cost function
loss_func = nn.NLLLoss()


#Validation 
def validation(model, val_loader, loss_func):
    
    val_loss = 0
    acc = 0
    
    for images, labels in iter(val_loader):
        
        images, labels = images.to(device), labels.to(device)
        
        output = model.forward(images)
        val_loss += loss_func(output, labels).item()
        
        probs = torch.exp(output)    #exp as we used LogSoftmax
        
        equality = (labels.data == probs.max(dim=1)[1])
        acc += equality.type(torch.FloatTensor).mean()
        
    return val_loss, acc


#Train the classifier
def train_classifier(epochs=50):
    
    steps = 0
    print_every = 50
    
    model.to(device)
    
    for e in range(epochs):
        
        model.train()
        
        for images, labels in iter(train_loader):
            
            steps += 1
            
            images, labels = images.to(device), labels.to(device)
            
            optimizer.zero_grad()   #recalc grad each loop
            
            output = model.forward(images)      #forward pass of the model
            loss = loss_func(output, labels)    #pass output and labels to the loss func
            loss.backward()                     #backprop that error
            optimizer.step()                    #update params
                              
            if steps % print_every == 0:
                model.eval()

                with torch.no_grad():
                    training_loss, train_accuracy = validation(model, train_loader, loss_func)
                    validation_loss, accuracy = validation(model, val_loader, loss_func)

                with open('training_logs/method11.txt', 'a') as f:    #CHANGE NAME OF OUTPUT FILE
                    print("Epoch: {}/{}: ".format(e+1, epochs),
                          "Training Loss: {:.3f} ".format(training_loss/len(train_loader)),
                          "Training Accuracy: {:.3f} ".format(train_accuracy/len(train_loader)),
                          "Validation Loss: {:.3f} ".format(validation_loss/len(val_loader)),
                          "Validation Accuracy: {:.3f}".format(accuracy/len(val_loader)),
                          file=f)

                model.train()
                    
train_classifier()

FILE = 'trained_models/method11.pth'
torch.save(model, FILE)
