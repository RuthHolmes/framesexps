Original Data Source: https://github.com/marlondcu/ISL

Hyperparams Across All VGG Models: 
| Hyperparameter        | Value                                  |
|-----------------------|----------------------------------------|
| Normalisation         | Pytorch recommendation for VGG network | 
| Image Resizing        | (120, 160)                             |
| Optimizer             | Adam                                   |
| Initial Learning Rate | 0.0001                                 | 
| Batch size            | 64                                     | 
| No. Epochs            | 50                                     |


Method Cheat Sheet: 
| Method | Signer Distribution | Frames     | Fine-tuning     | Augmentations          | 
|--------|---------------------|------------|-----------------|------------------------|
| 01     | Multi-signer        | Grey-scale | Last layer only | n/a                    |
| 02     | Signer-independent  | Grey-scale | Last layer only | n/a                    |
| 03     | Multi-signer        | Grey-sclae | All layers      | n/a                    |
| 04     | Signer-independent  | Grey-scale | All layers      | n/a                    |
| 05     | Signer-independent  | Grey-scale | All layers      | Mediapipe              |
| 06     | Signer-independent  | Grey-scale | Last layer only | Mediapipe              |
| 07     | Signer-independent  | RGB        | Last layer only | n/a                    |
| 08     | Signer-independent  | RGB        | Last layer only | Mediapipe              |
| 09     | Signer-independent  | RGB        | Last layer only | Blurred test set on 07 |
| 10     | Signer-independent  | RGB        | All layers      | n/a                    |
| 11     | Signer-independent  | RGB        | All layers      | Mediapipe              |
| 12     | Signer-independent  | RGB        | All layers      | Blurred test set on 10 |
